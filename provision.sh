#! /bin/bash

set -ex

yum -y install tar

cd /usr/local/src
curl -LO -b "oraclelicense=accept-securebackup-cookie" \
http://download.oracle.com/otn-pub/java/jdk/8u71-b15/jdk-8u71-linux-x64.tar.gz -o jdk-8u71-linux-x64.tar.gz
tar zxvf jdk-8u71-linux-x64.tar.gz

ln -s /usr/local/src/jdk1.8.0_71 /usr/local/java

echo "export JAVA_HOME=/usr/local/java" > /etc/profile.d/java.sh
echo "export PATH=\$PATH:\$JAVA_HOME/bin" >> /etc/profile.d/java.sh
